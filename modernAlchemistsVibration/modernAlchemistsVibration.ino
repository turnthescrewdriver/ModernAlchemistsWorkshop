/*
 * This example implements a very simple vibraton sensor as
 * a affordable motion sensor. It features reporting when
 * the microcontroller last experienced motion, as well as
 * setting time-limit that keeps the LED on for a period
 * of time after the microcontroller experienced motion.
 * 
 * This example takes a different approach yet again,
 * and includes the utilization of pin interrupts in 
 * order to have the microcontroller perform an action
 * when a certain event occurs.
 */

 // Required library imports
 #include <ArduinoJson.h>
 #include <ESP8266WiFi.h>
 #include <WiFiClient.h>
 #include <ESP8266WebServer.h>

 // Global variables
 ESP8266WebServer server(80); // Webserver
 const int interruptPin = 4; // The pin connect to the output of the vibration sensor
 bool everShaken = false; // Holds whether the device has experienced any motion at all
 int motionPeriod = 10000; // Holds the recent-motion threshold in milliseconds.
 /*
  * When using variables that can be changed with interrupts
  * ALWAYS use the `volatile` type. It tells the compiler
  * that these variables can change value at any time.
  * If you ever get weird behaviour when using interrupts,
  * it's probably because you forgot to do this.
  */
 volatile bool hasMotion = false; // Flag that indicates whether motion has occured within threshold
 volatile unsigned long lastMotion = 0; // Holds time when last motion was recorded

String HTML_Root = "<!doctype html><html><body>"
"<p>Hi there! Welcome to the Modern Alchemists NodeMCU Vibration sensor example program."
"This program demonstrates accessing sensor information through a REST API that returns and accepts JSON objects."
"This allows you to interact with your NodeMCU through something like NodeRED.</p>"
"<h1>API</h1>"
"<p>Send GET requests to /api/motion to get current motion status.<br>"
"Send POST requests to /api/motion/setThreshold with payload {\"newMotionThreshold\": x} to set the recent-motion threshold.</p>"
"</body> </html>";

 //Wi-Fi details.
 const char *ssid = "Broken";
 const char *password = "1648839316488393";

// Check if motion has occured, and then do things
 void checkIfMotionOccured(){
  // If motion has occured
  if(hasMotion){
    digitalWrite(LED_BUILTIN, 0); // Turn on the LED
    everShaken = true; // Set that the device has been shaken at least once

    // If the motion threshold has been exceeded
    if(millis() - lastMotion > motionPeriod){
      hasMotion = false; // Turn off the motion indicator flag
      digitalWrite(LED_BUILTIN, 1); // Turn of the LED
    }
  }
 }

/*
 * This is the interrupt routine that gets called
 * when the interrupt gets triggered. 
 * Always keep interrupt routines as short and
 * as fast as possible. Failing to do so will
 * end in having a really bad time.
 */
 void handleInterrupt(){
  hasMotion = true; // Set the motion flag
  lastMotion = millis(); // Record when motion happened
 }

/*
 * Callback functions are below.
 * There are functions that are called when the
 * ESP8266 receives a request to a certain route.
 * 
 * All the callback functions in this example deal
 * with JSON.
 */

// Root callback: /
 void handleRoot(){
  server.send(200, "text/html", HTML_Root); // Send response to client.
 }

// Motion callback: /api/motion
 void apiLastShake(){
  Serial.println("API REQUEST: Last shake"); // Debugging
  
  // Format JSON object and convert to String
  StaticJsonBuffer<200> jsonBuffer;
  JsonObject& data = jsonBuffer.createObject();
  char output[300];

  data["recentlyShaken"] = hasMotion; // Has motion flag
  data["RecentlyShakenThreshold"] = motionPeriod/1000; // Recent-motion threshold

  // If the device has been shaken at least once
  if(everShaken){
    data["lastShake_seconds"] = (millis()-lastMotion)/1000; // Calculate how long ago it was
  }
  else{ // If the device hasn't been shaken yet
    data["lastShake_seconds"] = "NoMotion"; // Report that no motion has been recorded.
  }

  data.printTo(output); // Convert to string
  server.send(200, "application/json", output); // Send response to client.
 }

// Set recent-motion threshold callback: /api/motionSetThreshold
 void apiSetRecentMotionThreshold(){
  Serial.println("API REQUEST: Set Recent Motion Threshold");
  
  // Retrieve and store JSON object
  StaticJsonBuffer<300> jsonBuffer;
  JsonObject& data = jsonBuffer.parseObject(server.arg("plain"));
  int newThresh = data["newMotionThreshold"];

  // Do some error-checking!
  // If the data wasn't provided at all
  if(!data.containsKey("newMotionThreshold")){
    server.send(400, "plain/text", "newMotionThreshold variable not received");
  }
  else if(newThresh < 0){ // If the new threshold is negative
    server.send(400, "plain/text", "newMotionThreshold must be positive value");
  }
  else{ // If the value is good
    motionPeriod = newThresh*1000; // Set new motion threshold.
    String output = "New recent motion threshold set to " + String(newThresh) + " seconds.";
    server.send(200, "plain/text", output); // Send respone to client.
  }
  
  
 }


/*
 * SETUP and LOOP functions are below.
 */
void setup() {
  Serial.begin(115200); // Begin serial
  pinMode(LED_BUILTIN, OUTPUT); // Set LED pin to output
  digitalWrite(LED_BUILTIN, 1); // Set the LED pin low to start with
  pinMode(interruptPin, INPUT); // Set interrupt pin as input.
  
  // Set the interrupt pin to call the function `handleInterrupt` when its value goes from low to high.
  attachInterrupt(digitalPinToInterrupt(interruptPin), handleInterrupt, RISING);

  //Initialize WiFi
  WiFi.begin(ssid,password);

  // Wait for connection
  while(WiFi.status() != WL_CONNECTED){
    delay(500);
    Serial.print(".");
  }

  //Map routes to the callback functions
  server.on("/", handleRoot);
  server.on("/api/motion", apiLastShake);
  server.on("/api/motion/setThreshold", apiSetRecentMotionThreshold);

  // Start server
  server.begin();

  // Print our connection info
  Serial.println("HTTP server started...");
  Serial.print("Connected to: ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

}


void loop() {
  server.handleClient(); // Handle the connections
  checkIfMotionOccured(); // Check if motion has occured
  delay(500); // We don't have to check for motion constantly.

}
