import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from betterGraphs import setLimits, setPrettyDefaults, addCustomTicks


t= np.arange(0,10,1)
vih = np.ones(10)*0.75*3.3
vil = np.ones(10)*0.25*3.3

setPrettyDefaults(context='talk', font_scale=2,linewidth=5, Tex=False)
fig = plt.figure()
ax = fig.add_subplot(111)
plt.plot(t, vih)
plt.plot(t, vil)

plt.yticks([0, 3.3])
plt.xticks([0, 10])
setLimits(ax)

addCustomTicks(ax, [2.475, 0.825, 3.3], ["VIH", "VIL", "VDD"], 'y')
addCustomTicks(ax, [10.0], ['t'], 'x')
sns.despine()

plt.xlabel('Time')
plt.ylabel('Voltage')

plt.savefig('logicLevel.pdf', dpi=300, bbox_inches='tight')
plt.show()
