import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from betterGraphs import setPrettyDefaults, setLimits, addCustomTicks


x = np.linspace(0,1,2000)
x2 = np.linspace(0,1,30)
y_a = np.sin(2*np.pi*x)
y_d = np.sin(2*np.pi*x2)

setPrettyDefaults(context='talk', Tex=False, linewidth=3, font_scale=1.5)

fig = plt.figure(figsize=[10,4])
ax = fig.add_subplot(111)
plt.plot(x,y_a, label='Analog')
plt.step(x2,y_d,where='pre', label='Digital')
plt.legend()
plt.xticks([0,1])
plt.yticks([-1,0,1])
plt.xlabel('Time')
plt.ylabel('Signal')

setLimits(ax)
sns.despine()
plt.tight_layout()
plt.savefig('analogvsdiscrete.pdf', bbox_inches='tight')
plt.show()
