'''
This file acts as a template for building beautiful graphs inspired by the
fantastic book "Trees, Maps and Theorems" by Jean-Luc Doumont.

It also includes a minimum-working example.

Can be integrated with TEX font rendering - works just fine.
'''

import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np

def setPrettyDefaults(palette='Set1', context='paper', font_scale=2, linewidth=2, Tex=True):
    '''
    Sets up good-looking default settings for matplotlib
    that form a good basis for 99% of graphs.

    Parameters
    ----------
    palette : string, optional
        Specifies the color palette to use. Uses Seaborn colours.
    context : string, optional
        Specifies the plot context. Uses Seaborn contexts.
    font_scale : int, optional
        Font-scaling to use with given context.
    linewidth: float, optional
        Linewidth to use when drawing plots.
    '''

    # Set template
    sns.set_style('ticks')
    sns.set_palette(palette)
    sns.set_context(context, font_scale=font_scale)

    # Set Matplotlib tweaks
    plt.rc('xtick', direction='in')
    plt.rc('ytick', direction='in')
    plt.rc('lines', linewidth=linewidth)

    if(Tex):
        plt.rc('text', usetex=True)
        plt.rc('font', serif='cm')
        plt.rc('font', family='serif')
        plt.rc('text.latex', preamble=r'\usepackage{amsmath}')

def addCustomTicks(ax, newLocs, newLabels, which='x'):

    if which == 'x':
        xticks = ax.get_xticks()
        ax.set_xticklabels(xticks)  # "Set" ticks must be done to access text
        locs, labels = plt.xticks()  # Get xticks
        labels = [x.get_text() for x in labels]  # Extract text from labels
    elif which == 'y':
        yticks = ax.get_yticks()
        ax.set_yticklabels(yticks)
        locs, labels = plt.yticks()
        labels = [x.get_text() for x in labels]

    dTicks = dict(zip(locs, labels))  # Place ticks in dictionary

    # Replace ticks with same tick location and add new ticks and labels
    for Loc, Lab in zip(newLocs, newLabels):
        dTicks[Loc] = Lab

    # Extract tick locations and labels
    locs = list(dTicks.keys())
    labels = list(dTicks.values())

    # Set new Ticks
    if which == 'x':
        ax.set_xticks(locs)
        ax.set_xticklabels(labels)
    elif which == 'y':
        ax.set_yticks(locs)
        ax.set_yticklabels(labels)


def setLimits(ax, x_factor=0.1, y_factor=0.1, spines=True):
    # x
    xticks = ax.get_xticks()
    ax.set_xticklabels(xticks)
    locs, labels = plt.xticks()

    x_max = np.max(locs)
    x_min = np.min(locs)

    yticks = ax.get_yticks()
    ax.set_yticklabels(yticks)
    locs, labels = plt.yticks()

    y_max = np.max(locs)
    y_min = np.min(locs)

    ax.set_xlim(x_min-x_max*x_factor, x_max*(x_factor+1))  # Sets x limits
    ax.set_ylim(y_min-y_max*y_factor, y_max*(y_factor+1))  # Sets y limits

    if spines:
        ax.spines['bottom'].set_bounds(x_min, x_max)
        ax.spines['left'].set_bounds(y_min, y_max)


# # Data for Plot
# x = np.arange(0, 100, 1)
# y1 = np.sin(x / 10)
# y2 = np.cos(x / 20)

# # Set Defaults without LaTeX font rendering
# setPrettyDefaults(Tex=False)

# # Plotting
# fig = plt.figure()
# ax = fig.add_subplot(111)
# plt.plot(x, y1, label='A')
# plt.plot(x, y2, label='B')
# plt.legend()

# # Customization
# sns.despine()

# # Regular ticks recommended to be limited to three points
# # This is unlike Trees, Maps and Theorems, which often shows only two.
# # Three points allows user to identify if graph is linear or log scale.
# plt.xticks([0, 50, 100])  # Xticks customized here
# plt.yticks([-1, 0, 1])  # Yticks customized here

# # Set plot limits roughly 10% above / below the max/min values respectively
# # and sets spines to match the maximum and minimum tick values
# setLimits(ax, x_factor=0.1, y_factor=0.1, spines=True)

# # Special ticks
# # Can be used to show units as well, which can then be ommitted from x/ylabel
# addCustomTicks(ax, [25, 75], ['0.25s', '0.75s'], 'x')
# addCustomTicks(ax, [0.75], ['0.75W'], 'y')

# # Axis labels
# plt.xlabel('Time')
# plt.ylabel('Power')

# plt.tight_layout()  # Fits plot nicely to screen
# plt.show()
