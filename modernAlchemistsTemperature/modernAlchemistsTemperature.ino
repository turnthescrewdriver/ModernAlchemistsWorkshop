/*
 * This example implements a temperature sensor,
 * more specifically the TMP36 analog temp. sensor.
 * It features maximum and minimum temperature 
 * logging as well as a rudimentary temperature
 * increase/descrease detection that can be accessed
 * using a REST API. 
 * 
 * This example also includes ESP8266-specific
 * workarounds for the noisy ADC due to the Wi-Fi
 * chip.
 */

// Required library imports
#include <ArduinoJson.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>

// Global variables
ESP8266WebServer server(80); // Webserver
int value; // Holds ADC readings
float tempC; // Holds temperature in Celsius
int tempValueMax=0; // Holds the maximum ADC reading from the TMP36
int tempValueMin=1024; // Holds the minimum ADC reading from the TMP36
int tempMaxTime=0; // Holds the number of seconds since the Max temperature was updated
int tempMinTime=0; // Holds the number of seconds since the Min temperature was updated
int tempMaxMin=0; // Holds the number of milliseconds since the Min temperature was updated.
unsigned long incrDecrInterval = 30000; // How often to check increasing/decreasing temperature in milliseconds
int lastTempValue = 0; // Holds previous temperature value for incr/decr temp function
int currTempValue = 0; // Holds current temperature value for incr/decr temp function
int currTime = millis(); // Holds the current time in milliseconds
int lastTime = millis(); // Holds the last time the temperature was checked for increasing/decreasing
bool isIncreasing = false; // True if temperature is increasing

//Set Wifi Credentials
const char *ssid = "Broken";
const char *password = "1648839316488393";

//Home Page HTML
String HTML_Root = "<!doctype html> <html><body>"
"<p>Hi there! Welcome to the Modern Alchemists NodeMCU TMP36 example program."
"This program demonstrates accessing sensor information either through"
"the HTML interface below, or through an API that returns JSON payloads, which you can combine with NodeRed.</p>"
"<h1>HTML interface</h1>\n"
"<p><b>Make sure you've hooked up the temperature sensor correctly - with output on pin A0.</b>"
"<p>Read <a href=\"/temp\">current temperature</a>.<br>"
"Read <a href=\"/temp/max\">maximum temperature</a>.<br>"
"Read <a href=\"/temp/min\">minimum temperature</a>.<br>"
"Read <a href=\"/temp/increasing\">whether temperature is increasing.</a></p>"
"<h1>API interface</h1>\n"
"<p>Send GET requests to /api/temp to get temperature.<br>"
"Send GET requests to /api/temp/max to get max temperature and how long ago it occured.<br>"
"Send GET requests to /api/temp/min to get min temperature and how long ago it occured.<br>"
"Send GET requests to /api/temp/increasing to get whether temperature is increasing or decreasing.<br>"
"</body> </html>";

/*
 * Using the ADC with Wi-Fi on introduces a lot of noise,
 * Particularly upward "spikes" in readings. This is
 * likely due to there not being a separate analog ground.
 * 
 * Workaround is to sample multiple times and use the
 * lowest value as the true reading.
 */
int getAnalogReading(){
  int reading;
  int readingBest = analogRead(A0);
  
  // Take multiple samples
  for(int i=0;i<25;i++){
    delay(10);
    reading = analogRead(A0);
    if (reading < readingBest){
      readingBest = reading; // Use the smallest value.
    }
  }
  return readingBest;
}

// Convert ADC reading to temperature for TMP36
float convertValueToTemp(int value){
  float volts = float(value)*1/1023; // convert to volts
  float celsius = (volts-0.5)/0.01; // convert to degrees celsius
  return celsius;
}

// Updates the global maximum temperature variable and how long ago it occured.
void updateMaxMinTemp(){
  int reading = getAnalogReading(); // Get temperature reading
  if(reading > tempValueMax){
    tempValueMax = reading; // Set new maximum temperature
  tempMaxTime = millis()/1000; //update time when max reading took place
  }
  else if(reading < tempValueMin){
    tempValueMin = reading; // Set new minimum temperature
    tempMinTime = millis()/1000; // Update time when min reading took place
  }
}

// Check and set whether temperature has increased or decreased within a set interval
void checkIncrDecrTemp(){
  currTime = millis(); // Update current time
  
  // If interval has been exceeded
  if(currTime - lastTime >= incrDecrInterval){

    currTempValue = getAnalogReading(); // Update current temperature reading
  
  // Compare to previous reading
  if(currTempValue > lastTempValue){
    isIncreasing = true;
  }
  else{
    isIncreasing = false;
  }
  
  lastTime = currTime; // Update time
  lastTempValue = currTempValue; // Set new "old" temperature reading
  }
}

/*
 * Callback functions are below.
 * These are functions that are called when the
 * ESP8266 receives a request to a certain route.
 */

 // Root callback: /
void handleRoot() {
  server.send ( 200, "text/html", HTML_Root ); // Send response to client
}

//Current Temperature callback: /temp
void handleTemp(){
  value = getAnalogReading(); //Read TMP36 temperature sensor.
  tempC = convertValueToTemp(value); // convert to degrees Celsius

  //Format HTML we want to display
  String output = "Temperature is " + String(tempC) + " degrees C.\n";
  server.send(200,"text/plain", output); // Send response to client.
}

// Maximum temperature callback: /temp/max
void handleTempMax(){
  float maxTempC = convertValueToTemp(tempValueMax); // Convert max temperature to celsius
  unsigned long timeSinceMax = millis()/1000 - tempMaxTime; // Calculate number of seconds since max was set
  String output = "Max temperature recorded was " + String(maxTempC) + "C. It occured " + String(timeSinceMax) + " seconds ago.";
  server.send(200, "text/plain", output); // Send response to client.
}

// Minimum temperature callback: /temp/min
void handleTempMin(){
  float minTempC = convertValueToTemp(tempValueMin); // Convert min temperature to celsius
  unsigned long timeSinceMin = millis()/1000 - tempMinTime;
  String output = "Min temperature recorded was " + String(minTempC) + "C. It occured " + String(timeSinceMin) + " seconds ago.";
  server.send(200, "text/plain", output); // Send response to client.
}

// Temperature increasing callback: /temp/increasing
void handleTempIncreasing(){
  String output;
  if(isIncreasing){
    output = "<p>The temperature is currently <b>increasing</b>!";
  }
  else{
    output = "<p>The temperature is currently <b>decreasing</b>!";
  }
  unsigned long lastCheck = (millis()-lastTime)/1000;
  output = output + "<br>Check occurs every " + String(incrDecrInterval/1000) + " seconds.";
  output = output + "<br>Last check was " + String(lastCheck) + " seconds ago.</p>";

  server.send(200, "text/html", output);
}

/*
 * JSON-returning API callbacks.
 * These perform an identical function the previous callbacks,
 * except they demonstrate how to return results in a JSON
 * format using ArduinoJSON.h for easy(er) processing via 
 * something like NodeRED or another service.
 */

// Temperature API: /api/temp
void apiTemp(){
  Serial.println("API REQUEST: Read temperature"); // Nice for debugging
  value = getAnalogReading(); // Read TEMP36 temperature sensor
  tempC = convertValueToTemp(value); // Convert to degrees celsius
  // Write to JSON object and format as string
  StaticJsonBuffer<200> jsonBuffer;
  JsonObject& data = jsonBuffer.createObject();
  char output[300]; // Output string
  data["temperature"] = tempC;
  data.printTo(output); // Convert to string for sending back to client

  server.send(200, "application/json", output); // Send response to client.
}

// Temperature Max API: /api/temp/max
void apiTempMax(){
  Serial.println("API REQUEST: Max Temperature");
  // Write to JSON object and format as string
  StaticJsonBuffer<200> jsonBuffer;
  JsonObject& data = jsonBuffer.createObject();
  char output[300]; // Output string
  data["maxTemp"] = convertValueToTemp(tempValueMax); // Max temperature
  data["secondsAgo"] = millis()/1000 - tempMaxTime; // Calculate number of seconds ago it was measured.
  data.printTo(output); // Convert to string for sending back to client

  server.send(200, "application/json", output); // Send response to client.
}

// Temperature Min API: /api/temp/min
void apiTempMin(){
  Serial.println("API REQUEST: Min Temperature");
  //Write to JSON object ans format as string
  StaticJsonBuffer<200> jsonBuffer;
  JsonObject& data = jsonBuffer.createObject();
  char output[300]; // Output string
  data["minTemp"] = convertValueToTemp(tempValueMin); // Min temperature
  data["secondsAgo"] = millis()/1000 - tempMinTime; // Calculate number of seconds ago it was measured
  data.printTo(output); // Convert to string for sending back to client

  server.send(200, "application/json", output); // Send response to client.
}

// Temperature Increasing/Decreasing API: /api/temp/increasing
void apiTempIncreasing(){
  Serial.println("API REQUEST: Temperature Increasing");
  //Write to JSON object ans format as string
  StaticJsonBuffer<200> jsonBuffer;
  JsonObject& data = jsonBuffer.createObject();
  char output[300]; // Output string
  data["increasing"] = isIncreasing; // Whether temperature is increasing or decreasing
  data["last_check_seconds"] = (millis()-lastTime)/1000; // When the last check occured
  data["checkInterval"] = incrDecrInterval/1000; // How often checks are made
  data.printTo(output); // Convert to string for sending back to client

  server.send(200, "application/json", output); // Send response to client.
}


 
/*
 * SETUP and LOOP functions are below.
 */
void setup() {
  pinMode(A0, INPUT); // Set the ADC pin as input (only have one on the ESP8266)
  Serial.begin(115200); // Initialize serial

  //Initialize WiFi
  WiFi.begin(ssid,password);

  // Wait for connection
  while(WiFi.status() != WL_CONNECTED){
    delay(500);
    Serial.print(".");
  }

  // Map routes to the callback functions
  server.on("/", handleRoot);
  server.on("/temp", handleTemp);
  server.on("/temp/max", handleTempMax);
  server.on("/temp/min", handleTempMin);
  server.on("/temp/increasing", handleTempIncreasing);
  server.on("/api/temp", apiTemp);
  server.on("/api/temp/max", apiTempMax);
  server.on("/api/temp/min", apiTempMin);
  server.on("/api/temp/increasing", apiTempIncreasing);
  

  // Start server
  server.begin();

  // Print our connection info
  Serial.println("HTTP server started...");
  Serial.print("Connected to: ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());


}

void loop() {
  server.handleClient(); // Let the ESP8266 handle connections
  updateMaxMinTemp(); // Update the minimum and maximum temperatures
  checkIncrDecrTemp(); // Check whether to set whether temperature is increasing or decreasing
  delay(1000); // Don't need to check temperature super often

}
