/*
 * This is a basic example illustrating how to sample from the ADC
 * and how to make the sensor data available with an API.
 */
//Import Libraries
#include <ArduinoJson.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>


//Global variables
ESP8266WebServer server ( 80 ); // Webserver
int value; // Holds 10-bit ADC readings
float volts; // Holds ADC value converted to voltage
float temp; // Holds temperature in Celsius
float lightLevel; // Holds light level as a percentage

//Set Wifi Credentials
//Change to your network settings!
const char *ssid = "Broken";
const char *password = "1648839316488393";

//Home Page HTML
String HTML_Root = "<!doctype html> <html><body>"
"<p>Hi there! Welcome to the Modern Alchemists NodeMCU example program."
"This program demonstrates accessing sensor information either through"
"the HTML interface below, or through an API that returns JSON payloads.</p>\n"
"<h1>HTML interface</h1>\n"
"<p><b>Make sure you've only wired up the temperature sensor OR the light level sensor - NOT BOTH!</b></p>"
"<p>Read <a href=\"/temp\">temperature </a>.<br>"
"Read <a href=\"/light\">light level</a>.<br>"
"Read <a href=\"/shaken-or-stirred\">shake state</a></p>"
"<h1>API interface</h1>\n"
"<p>Send GET requests to /api/temp to get temperature.<br>"
"Send GET requests to /api/light to get light level.</p>"
"</body> </html>";

/*
 * Using the ADC with Wi-Fi on introduces a lot of noise,
 * Particularly upward "spikes" in readings. This is
 * likely due to there not being a separate analog ground.
 * 
 * Workaround is to sample multiple times and use the
 * lowest value as the true reading.
 */
int getAnalogReading(){
  int reading;
  value = analogRead(A0);

  // Take multiple readings
  for(int i=0;i<25;i++){
    delay(10);
    reading = analogRead(A0);
    if (reading < value){
      value = reading; // Choose lowest value
    }
  }

  return value;
}

//Callback Functions
//Root callback
void handleRoot() {
  server.send ( 200, "text/html", HTML_Root );
}

//Temperature callback
void handleTemp(){
  value = analogRead(A0); //Read TMP36 temperature sensor.
  volts = float(value)*1/1024; // convert to volts
  temp = (volts-0.5)/0.01; // convert to degrees Celsius

  //Format HTML we want to display
  String output = "Temperature is " + String(temp) + " degrees C.\n";
  server.send(200,"text/html", output); // Send to client
}

//Light callback
void handleLight(){
  value = analogRead(A0); // Read voltage of LDR
  lightLevel = float(value)/1024*100; // Convert light level to a percentage
  String output = "Current light level is " + String(lightLevel) + "%.\n"; // Format data we want to display

  // A little bit of fun.
  if(lightLevel > 50.0){
    output = output + "<p>It's rather light right now.</p>";
  }
  else{
    output = output + "<p>It's rather dark right now.</p>";
  }
  
  server.send(200, "text/html", output); // Send to client
}

//Shake callback
void handleShake(){
  server.send(200, "text/html", "WIP. Placeholder");
}

// API temperature
void apiTemp(){
  Serial.println("API REQUEST: Temperature");
  value = getAnalogReading(); //Read TMP36 temperature sensor.
  volts = float(value)*1/1024; // convert to volts
  temp = (volts-0.5)/0.01; // convert to degrees Celsius

  // Write to JSON object and format as string
  StaticJsonBuffer<200> jsonBuffer;
  JsonObject& root = jsonBuffer.createObject();
  char output[300];
  root["temperature"] = temp;
  root.printTo(output);

  server.send(200, "application/json", output); // Send to client
}

// API light
void apiLight(){
  Serial.println("API REQUEST: Light");
  value = analogRead(A0); // Read voltage of LDR
  lightLevel = float(value)/1024*100; // Convert light level to a percentage

  StaticJsonBuffer<200> jsonBuffer;
  JsonObject& root = jsonBuffer.createObject();
  char output[300];
  root["light_percentage"] = lightLevel;
  root.printTo(output);
  server.send(200, "application/json", output);
}

//Page Not Found Callback
void handleNotFound() {
  server.send ( 404, "text/plain", "Error! Not Found\n\n" );
}

void setup() {
  pinMode(A0, INPUT); //Set ADC pin (we only have one on the board)
  Serial.begin ( 115200 ); // Initialize serial

  //Initialize Wifi
  WiFi.begin ( ssid, password );

  // Wait for connection and print status to serial
  while ( WiFi.status() != WL_CONNECTED ) {
      delay ( 500 );
      Serial.print ( "." );
    }

  //Map Routes
  server.on( "/", handleRoot );
  server.on("/temp", handleTemp);
  server.on("/light", handleLight);
  server.on("/shaken-or-stirred", handleShake);
  server.on("/api/temp", apiTemp);
  server.on("/api/light", apiLight);
  server.onNotFound ( handleNotFound );
  
  //Begin Server
  server.begin();

  //Print Connection
  Serial.println ( "HTTP server started\n" );
  Serial.print ( "Connected to " );
  Serial.println ( ssid );
  Serial.print ( "IP address: " );
  Serial.println ( WiFi.localIP() );
}

//Main loop
void loop() {
    server.handleClient();
}
