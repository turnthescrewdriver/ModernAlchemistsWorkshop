/*
 * This example implements a more advanced API and sensor-reading
 * techniques. It features maximum temperature logging and
 * temperature increase / decrease detection that can be
 * accessed using a REST API.
 * 
 * This example also implements a better ADC sampling method
 * that helps eliminate the ADC anomolies caused by the WiFi 
 * chip.
 */

//Import Libraries
#include <ArduinoJson.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>


//Global variables
ESP8266WebServer server ( 80 ); // Webserver
int value; // Holds 10-bit ADC readings
int tempValue; // Holds the 10-bit ADC readings from the TMP36
int valueTempMax=0; // Holds the maximum ADC reading of the TMP36
int valueTempMin=1024; // Holds the minimum ADC reading of the TMP36
int tempMaxTime=0; // Holds the number of milliseconds since the Max temperature was updated
int tempMinTime=0; // Holds the number of milliseconds since the Min temperature was updated
unsigned long incrDecrInterval = 30000; // Temperature Increasing/Decreasing interval in milliseconds
int lastTempValue = 0; // Holds previous temperature value for incr/decr temp function
int currTempValue = 0; // Holds current temperature value for incr/decr temp function
int currTime = millis(); // Holds the current time in milliseconds
int lastTime = millis(); // Holds the last time the temperature was checked for increasing/decreasing
bool isIncreasing = false; // True if temperature is increasing
float temp; // Holds temperature in Celsius
float volts; // Holds value of ADC converted to volts
float lightLevel; // Holds light level as a percentage

//Set Wifi Credentials
const char *ssid = "YourWifiHere";
const char *password = "YourWifiPasswordHere";

//Home Page HTML
String HTML_Root = "<!doctype html> <html><body>"
"<p>Hi there! Welcome to the Modern Alchemists NodeMCU example program."
"This program demonstrates accessing sensor information either through"
"the HTML interface below, or through an API that returns JSON payloads.</p>\n"
"<h1>HTML interface</h1>\n"
"<p><b>Make sure you've only wired up the temperature sensor OR the light level sensor - NOT BOTH!</b></p>"
"<p>Read <a href=\"/temp\">temperature </a>.<br>"
"Read <a href=\"/light\">light level</a>.<br>"
"Read <a href=\"/shaken-or-stirred\">shake state</a></p>"
"<h1>API interface</h1>\n"
"<p>Send GET requests to /api/temp to get temperature.<br>"
"Send GET requests to /api/temp/max to get max temperature and how long ago it occured.<br>"
"Send GET requests to /api/temp/min to get min temperature and how long ago it occured.<br>"
"Send GET requests to /api/temp/increasing to get whether temperature is increasing or decreasing.<br>"
"Send GET requests to /api/light to get light level.</p>"
"</body> </html>";

/*
 * Using the ADC with Wi-Fi on introduces a lot of noise,
 * Particularly upward "spikes" in readings. This is
 * likely due to there not being a separate analog ground.
 * 
 * Workaround is to sample multiple times and use the
 * lowest value as the true reading.
 */
int getAnalogReading(){
  int reading;
  value = analogRead(A0);
  
  // Take multiple samples
  for(int i=0;i<25;i++){
    delay(10);
    reading = analogRead(A0);
    if (reading < value){
      value = reading; // Use the smallest value.
    }
  }
  return value;
}

// Convert ADC reading to temperature for TMP36
float convertValueToTemp(int value){
  volts = float(value)*1/1024; // convert to volts
  temp = (volts-0.5)/0.01; // convert to temperature
  return temp;
}

// Convert ADC reading into a percentage of max (1.0V)
float convertValuetoPerc(int value){
  float percent = float(value)/1024*100;
  return percent;
}

// Updates the global maximum temperature variable and how long ago it occured.
void updateMaxMinTemp(){
  int reading = getAnalogReading(); // Get temperature reading
  if(reading > valueTempMax){
    valueTempMax = reading; // Set new maximum temperature
	tempMaxTime = millis()/1000; //update time when max reading took place
  }
  else if(reading < valueTempMin){
    valueTempMin = reading; // Set new minimum temperature
    tempMinTime = millis()/1000; // Update time when min reading took place
  }
}

// Check whether temperature has increased or decreased within a set interval
void checkIncrDecrTemp(){
  currTime = millis(); // Update current time
  
  // If interval has been exceeded
  if(currTime - lastTime >= incrDecrInterval){
	lastTime = currTime; // Update time
    currTempValue = getAnalogReading(); // Update current temperature reading
	
	// Compare to previous reading
	if(currTempValue > lastTempValue){
		isIncreasing = true;
	}
	else{
		isIncreasing = false;
	}
	
	lastTempValue = currTempValue; // Set new "old" temperature reading
  }
}

//Callback Functions
//Root callback
void handleRoot() {
  server.send ( 200, "text/html", HTML_Root );
}

//Temperature callback
void handleTemp(){
  value = getAnalogReading(); //Read TMP36 temperature sensor.
  temp = convertValueToTemp(value); // convert to degrees Celsius

  //Format HTML we want to display
  String output = "Temperature is " + String(temp) + " degrees C.\n";
  server.send(200,"text/html", output); // Server sends data
}

//Light callback
void handleLight(){
  value = analogRead(A0); // Read voltage of LDR
  lightLevel = float(value)/1024*100; // Convert light level to a percentage
  String output = "Current light level is " + String(lightLevel) + "%.\n"; // Format data we want to display

  // A little bit of fun.
  if(lightLevel > 50.0){
    output = output + "<p>It's rather light right now.</p>";
  }
  else{
    output = output + "<p>It's rather dark right now.</p>";
  }
  
  server.send(200, "text/html", output); // Send to server
}

//Shake callback
void handleShake(){
  server.send(200, "text/html", "Placehold - WIP!");
}

// API temperature
void apiTemp(){
  Serial.println("API REQUEST: Read Temperature");
  value = getAnalogReading(); //Read TMP36 temperature sensor.
  temp = convertValueToTemp(value); // convert to degrees Celsius

  StaticJsonBuffer<200> jsonBuffer;
  JsonObject& root = jsonBuffer.createObject();
  char output[300];
  root["temperature"] = temp;
  root.printTo(output);

  server.send(200, "application/json", output);
}

// Return whether temperature is increasing or decreasing
// Include time since last check and the check interval.
void apiTempIncreasing(){
	Serial.println("API REQUEST: Temperature Gradient");
	StaticJsonBuffer<200> jsonBuffer;
	JsonObject& mesg = jsonBuffer.createObject();
	char output[200];
	
	mesg["temp_increasing"] = isIncreasing;
	mesg["last_check_seconds"] = (millis()-lastTime)/1000;
	mesg["check_interval_seconds"] = incrDecrInterval/1000;
	mesg.printTo(output);
	Serial.println(output);
	
	server.send(200, "application/json", output);
  
}

// API max temperature
void apiTempMax(){
  Serial.println("API REQUEST: Max Temperature");
  // Write to JSON object and format as string
  StaticJsonBuffer<200> jsonBuffer;
  JsonObject& mesg = jsonBuffer.createObject();
  char output[200];
  mesg["maxTemp"] = convertValueToTemp(valueTempMax); // Maximum temperature
  mesg["seconds_ago"] =  millis()/1000 - tempMaxTime; // Calculate number of seconds ago it occured
  mesg.printTo(output); // Convert to string
  
  server.send(200, "application/json", output); //Send response to client
  
}

// API min temperature
void apiTempMin(){
  Serial.println("API REQUEST: Min Temperature");
  // Write to JSON object and format as string
  StaticJsonBuffer<200> jsonBuffer;
  JsonObject& mesg = jsonBuffer.createObject();
  char output[200];
  mesg["min_temp"] = convertValueToTemp(valueTempMin); // Minimum temperature
  mesg["seconds_ago"] = millis()/1000 - tempMinTime; // Calculate number of seconds ago it occured
  mesg.printTo(output); // Convert to string

  server.send(200, "application/json", output); // Send response to client
}

// API light
void apiLight(){
  Serial.println("API REQUEST: Light");
  value = getAnalogReading(); // Read voltage of LDR
  lightLevel = convertValuetoPerc(value); // Convert light level to a percentage

  // Write to JSON object and format as string
  StaticJsonBuffer<200> jsonBuffer;
  JsonObject& mesg = jsonBuffer.createObject();
  char output[200];
  mesg["light_percentage"] = lightLevel;
  mesg.printTo(output);
  server.send(200, "application/json", output);
}

//Page Not Found Callback
void handleNotFound() {
  server.send ( 404, "text/plain", "Error! Page not found." );
}

void setup() {
  pinMode(A0, INPUT); //Set ADC pin (we only have one on the board)
  Serial.begin ( 115200 ); // Initialize serial

  //Initialize Wifi
  WiFi.begin ( ssid, password );

  // Wait for connection and print status to serial
  while ( WiFi.status() != WL_CONNECTED ) {
      delay ( 500 );
      Serial.print ( "." );
    }

  //Map Routes
  server.on( "/", handleRoot );
  server.on("/temp", handleTemp);
  server.on("/light", handleLight);
  server.on("/shaken-or-stirred", handleShake);
  server.on("/api/temp", apiTemp);
  server.on("/api/temp/increasing", apiTempIncreasing);
  server.on("/api/temp/max", apiTempMax);
  server.on("/api/temp/min", apiTempMin);
  server.on("/api/light", apiLight);
  server.onNotFound ( handleNotFound );
  
  //Begin Server
  server.begin();

  //Print Connection
  Serial.println ( "HTTP server started\n" );
  Serial.print ( "Connected to " );
  Serial.println ( ssid );
  Serial.print ( "IP address: " );
  Serial.println ( WiFi.localIP() );
}

//Main loop
void loop() {
    server.handleClient();
    updateMaxMinTemp(); // Update maximum temperature
	checkIncrDecrTemp(); // Do increasing/decreasing check
	delay(1000);
}
