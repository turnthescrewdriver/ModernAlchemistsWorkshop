/*
 * This example implements a LDR (light-dependent resistor)
 * as an affordable light-level sensor. It features reporting
 * the current light level as a percentage, and facilitates
 * sending a command to set a low-light level threshold, which 
 * when reached will toggle an LED.
 * 
 * This example takes a slightly different approach as well.
 * Instead of only sampling the sensor when the ESP8266
 * receives a request, we are going to constantly sample the
 * light sensor and simply return the light level when asked.
 * 
 * This example includes ESP8266-specific workaround for
 * the noisy ADC due to the Wi-Fi chip.
 */

// Required library imports
#include <ArduinoJson.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>

//Global variables
ESP8266WebServer server(80); // Webserver
int lightValue; // Holds ADC readings
int lowLightThresh=50; // Holds the low-light level threshold as a percentage
int currentLight=0; // Holds the current light level as a percentage

const char *ssid = "Broken";
const char *password = "1648839316488393";

// Home page HTML
String HTML_Root = "<!doctype html><html><body>"
"<p>Hi there! Welcome to the Modern Alchemists NodeMCU LDR example program."
"This program demonstrates acessing sensor information through a REST API that returns and accepts JSON objects."
"This allows you to interact with your NodeMCU through something like NodeRED.</p>"
"<h1>API</h1>"
"<p>Send GET requests to /light to get current light level as a percentage.<br>"
"Send GET request to /light/threshold to get the current low-light level threshold.<br>"
"Send POST requests to /light/setThreshold with payload {\"newThreshold\": 0<x<100} to set the light-level threshold. Drop below this level and the LED will turn on!</p>"
"</body> </html>";

/*
 * Using the ADC with Wi-Fi on introduces a lot of noise,
 * Particularly upward "spikes" in readings. This is
 * likely due to there not being a separate analog ground.
 * 
 * Workaround is to sample multiple times and use the
 * lowest value as the true reading.
 */
int getAnalogReading(){
  int reading;
  int readingBest = analogRead(A0);
  
  // Take multiple samples
  for(int i=0;i<25;i++){
    delay(10);
    reading = analogRead(A0);
    if (reading < readingBest){
      readingBest = reading; // Use the smallest value.
    }
  }
  return readingBest;
}

// Check if the light-level is below the threshold, and if so, turn on LED

void checkThreshold(){
  float currLight = convertToPercentage(lightValue); // Get current light value as percentage.
  
  if(currLight < float(lowLightThresh)){ // Turn on LED if light is below threshold
    digitalWrite(LED_BUILTIN, 0);
  }
  else{ // Turn off LED if light is above threshold
    digitalWrite(LED_BUILTIN, 1);
  }
}

// Convert ADC reading to a percentage
float convertToPercentage(int val){
  return val/1023.0*100; // Convert to percentage
}

/*
 * Callback functions are below.
 * There are functions that are called when the
 * ESP8266 receives a request to a certain route.
 * 
 * All the callback functions in this example deal
 * with JSON.
 */

 // Root callback: /
 void handleRoot(){
  server.send(200, "text/html", HTML_Root);
 }
 
 // Light reading callback: /api/light
 void apiLight(){
  Serial.println("API REQUEST: Light"); // Nice for debugging

  // Write to JSON object and format as string
  StaticJsonBuffer<200> jsonBuffer;
  JsonObject& data = jsonBuffer.createObject();
  char output[300]; // Output string
  data["lightLevel"] = convertToPercentage(lightValue); // Calculate light value as percentage
  data.printTo(output);

  server.send(200, "application/json", output); // Send response to client.
 }

 // Get light threshold callback: /api/light/threshold
 void apiGetThreshold(){
  Serial.println("API REQUEST: Get light threshold"); // Debugging

  // Write to JSON object and format as string.
  StaticJsonBuffer<200> jsonBuffer;
  JsonObject& data = jsonBuffer.createObject();
  char output[300]; // Output string
  data["threshold"] = lowLightThresh; // Current low-light threshold.
  data.printTo(output);

  server.send(200, "application/json", output); // Send response to client.
 }

 // Set light threshold callback: /api/light/setThreshold
 void apiSetThreshold(){
  Serial.println("API REQUEST: Set light threshold"); // Debugging

  // Retrieve and store JSON object
  StaticJsonBuffer<300> jsonBuffer;
  JsonObject& data = jsonBuffer.parseObject(server.arg("plain"));
  int intThresh = data["newThreshold"]; // Extract new threshold value
  
  // Do some error-checking!
  // Check if the data was sent at all
  if(!data.containsKey("newThreshold")){
    server.send(400, "plain/text", "newThreshold variable not received.");
  }
  else if(intThresh > 100){ // If the threshold was too large
    server.send(418, "text/plain", "Invalid input (too large). Also, I'm a teapot"); // Send response to client.
  }
  else if(intThresh < 0){ // If the threshold was negative
    server.send(418, "text/plain", "Invalid input (negative)."); // Send response to client.
  }
  else{ // If everything is in order
    lowLightThresh = intThresh;
    String output = "New threshold set to " + String(intThresh);
    server.send(200, "text/plain", output); // Send response to client.
  }
 }

/*
 * SETUP and LOOP functions are below.
 */
void setup() {
  pinMode(A0, INPUT); // Set the ADC pin as input (only have one on the ESP8266)
  pinMode(LED_BUILTIN, OUTPUT); // Set the built-in LED as output.
  digitalWrite(LED_BUILTIN, 1); // Turn off the LED to start with.
  Serial.begin(115200); // Initialize serial

  //Initialize WiFi
  WiFi.begin(ssid,password);

  // Wait for connection
  while(WiFi.status() != WL_CONNECTED){
    delay(500);
    Serial.print(".");
  }

  //Map routes to the callback functions
  server.on("/", handleRoot);
  server.on("/light", apiLight);
  server.on("/light/threshold", apiGetThreshold);
  server.on("/light/setThreshold", apiSetThreshold);

  // Start server
  server.begin();

  // Print our connection info
  Serial.println("HTTP server started...");
  Serial.print("Connected to: ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

}

void loop() {
  server.handleClient(); // Let the ESP8266 handle connections.
  lightValue = getAnalogReading();
  checkThreshold();
  delay(1000);

}
